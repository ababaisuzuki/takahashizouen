/**
 * @file addHoverClass
 * タッチデバイス以外でhoverのクラスを付与
 */
;(function($) {
  $.fn.addHoverClass = function() {
    var $el = $(this);

    $el.hover(function() {
      $(this).addClass('hover');
    }, function() {
      $(this).removeClass('hover');
    });
  };
})(jQuery);

/**
 * scrollAddClass
 * スクロール後に要素にクラスを追加するプラグイン
 */
;(function() {
  $.fn.scrollAddClass = function(options) {
    var scrollAddClassOptions = $.extend( {}, $.fn.scrollAddClass.defaults, options );
   
    var $el = $(this);
    var $win = $(window);

    var className = scrollAddClassOptions.className;
    var devideHeight = scrollAddClassOptions.devideHeight;
    var isScroll = false;
    var isEnd = false;

    function onScroll() {
      if ( !isScroll ) {
        requestAnimationFrame(function() {
          isScroll = false;
          runAddClass();
        });
        isScroll = true;
      }
    }

    function runAddClass() {
      if ( isEnd ) return;
      if ( $win.scrollTop() > $el.offset().top - window.innerHeight / devideHeight ) {
        $el.addClass(className);
        isEnd = true;
      }
    }

    window.addEventListener('scroll', onScroll);
    runAddClass();

  };

  $.fn.scrollAddClass.defaults = {
    className: 'is-scrollIn',
    devideHeight: 1.5
  };
})(jQuery);

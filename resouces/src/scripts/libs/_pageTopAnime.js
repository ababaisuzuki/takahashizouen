/**
 * @file pageTopAnime
 * pageTop
 */
;(function($) {
  $.fn.pageTopAnime = function(options) {
    var pageTopAnimeOptions = $.extend({}, $.fn.pageTopAnime.defaults, options);

    var $el = $(this);
    var $win = $(window);

    var easing = pageTopAnimeOptions.easing,
        speed = pageTopAnimeOptions.speed;

    var isHover = false;
    var isTouchDevice = (window.ontouchstart === null) ? true : false;
    var isScroll = false;


    /**
     * スクロール時に実行
     */
    function onScroll() {
      if ( !isScroll ) {
        requestAnimationFrame(function() {
          isScroll = false;
          fixedPageTop();
        });
        isScroll = true;
      }
    }

    /**
     * ページトップボタン表示
     */
    function fixedPageTop() {
      if ( $win.scrollTop() > 200 && $win.scrollTop() <= $('#js-footer-navi').offset().top - window.innerHeight  ) {
        if ( $el.hasClass('is-fixed') ) return;
        $el.addClass('is-fixed');
      }

      if ( $win.scrollTop() > $('#js-footer-navi').offset().top - window.innerHeight ) {
        if ( !($el.hasClass('is-fixed')) ) return;
        $el.removeClass('is-fixed');
      }

      if ( $win.scrollTop() <= 200 ) {
        if ( !($el.hasClass('is-fixed')) ) return;
        $el.removeClass('is-fixed');
      }
    }

    if ( !isTouchDevice ) {
      $el.hover(function() {
        if ( !($(this).hasClass('hover')) ) {
          $(this).addClass('hover');
          isHover = true;
        }
      }, function() {
        if ( $(this).hasClass('hover') ) {
          $(this).removeClass('hover');
          isHover = false;
        }
      });
    }

    $el.on('click', function() {
      if ( isHover ) {
        $el.removeClass('hover');
      }

      $('#js-wrapper').velocity('scroll', {
        easing: easing,
        duration: speed,
        offset: 0,
        mobileHA: false,
        begin: function() {
          $el.addClass('is-fixed');
        },
        complete: function() {
          $el.removeClass('is-fixed');
        }
      });

    });

    window.addEventListener('scroll', onScroll, { passive: false });

  };


  $.fn.pageTopAnime.defaults = {
    easing: 'easeOutQuad',
    speed: 880
  };
})(jQuery);

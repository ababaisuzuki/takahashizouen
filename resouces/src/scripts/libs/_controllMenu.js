/**
 * @file controllMenu
 * メニューの開閉を管理
 */
;(function($) {
  $.fn.controllMenu = function(options) {
    var controllMenuOptions = $.extend( {}, $.fn.controllMenu.defaults, options );
    var $el = $(this);
    var $html = $('html');
    var $body = $('body');
    var $open = $(controllMenuOptions.$open);
    var $close = $el.find(controllMenuOptions.$close);
    var $scrollLink = $(controllMenuOptions.$scrollLink);
    var $spSticky = $('#js-sp-sticky');
    var $stickyBtn = $('#js-sticky-btn');

    var animeSpeed = controllMenuOptions.animeSpeed;
    var easing = controllMenuOptions.easing;
  
    var HEADER_HEIGHT = 0;
    var isOpen = false;
    var isAnimation = false;
    var hasParam = ( location.search.length > 0 && location.search.indexOf('?link=')!==-1 );


    /**
     * メニューを表示
     */
    function openMenu() {
      $el.addClass('is-show');
      $html.addClass('is-hide-scroll');
      $body.addClass('is-hide-scroll');
      $spSticky.addClass('is-hidden');
      $stickyBtn.addClass('is-hidden');
      $open.addClass('is-hidden');
      isAnimation = false;
    }

    /**
     * メニューを非表示
     */
    function closeMenu() {
      $el.removeClass('is-show');
      $html.removeClass('is-hide-scroll');
      $body.removeClass('is-hide-scroll');
      $spSticky.removeClass('is-hidden');
      $stickyBtn.removeClass('is-hidden');
      $open.removeClass('is-hidden');
      isAnimation = false;
    }


    /**
     * ページ内スクロール
     * @param {string} _target スクロール先のID名 
     */
    function scrollToPosition(_target) {
      $('html, body').stop().animate({
        scrollTop: $('#' + _target).offset().top - HEADER_HEIGHT
      }, animeSpeed, easing);
    }


    $open.on('click', function() {
      if ( isAnimation ) return false;
      isOpen = true;
      isAnimation = true;
      openMenu();
    });

    $close.on('click', function() {
      if ( isAnimation ) return false;
      isOpen = false;
      isAnimation = true;
      closeMenu();
    });

    $scrollLink.on('click', function(e) {
      e.preventDefault();
      var param = $(this).attr('href').replace(/\?link=/g, '');
      closeMenu();
      scrollToPosition(param);
    });

    // パラメータ付きのリンクなら該当のIDまでスクロール
    if ( hasParam ) {
      window.addEventListener('DOMContentLoaded', function() {
        var param = location.search.replace(/\?link=/g, '');
        scrollToPosition(param);
      });
    }

  };

  $.fn.controllMenu.defaults = {
    $open: '.js-open-menu',
    $close: '.js-close-menu',
    $scrollLink: '.js-scrollLink',
    animeSpeed: 300,
    easing: 'swing'
  };
})(jQuery);

/**
 * タッチデバイス以外でホバーのクラスを付与
 */
$(function() {
  // タッチデバイス判定
  var isTouchDevice =  window.ontouchstart === null;
  
  var $hover = $('.js-hover');

  if ( !isTouchDevice ) {
    $hover.addHoverClass();
  }
});

/**
 * メニュー開閉
 */
$(function() {
  var $navi = $('#js-navi');

  var controllMenuOptions = {
    $open: '.js-open-menu',
    $close: '.js-close-menu',
    animeSpeed: 300,
    easing: 'swing'
  };

  $navi.controllMenu(controllMenuOptions);
});


/**
 * スクロール後にフェードイン
 */
$(function() {
  var $scrollIn = $('.js-scrollIn');
  var scrollInOptions = {
    className: 'is-scrollIn',
    devideHeight: 1.1
  }

  $scrollIn.each(function() {
    $(this).scrollAddClass(scrollInOptions);
  });
});


/**
 * ページトップ
 */
$(function() {
  var $pageTop = $('#js-pagetop');
  $pageTop.pageTopAnime();
});


/**
 * object-fit ie用
 */
$(function() {
  objectFitImages('img.img-fit');
});


/**
 * 電話起動タッチデバイスのみ
 */
$(function() {
  var isTouchDevice =  window.ontouchstart === null;
  var $spTel = $('.js-sp-tel');

  if ( !isTouchDevice ) {
    $spTel.addClass('is-no-event');
    $spTel.on('click', function(e) {
      e.preventDefault();
    });
  }
});

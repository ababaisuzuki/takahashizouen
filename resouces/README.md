# フロントテンプレート

## ファイル構成

```
├── README.md 
├── gulpfile.js　
├── package.json
├── src
│   ├── images ・・・画像ファイル。
│   ├── scripts ・・・スクリプトファイル。
│   ├── styles ・・・スタイルシート(scss)。コマンド実行でcssに変換。
│   └── svg ・・・svg形式のファイル。コマンド実行でスプライト化。
└── yarn.lock
```

## 使い方

`resouces/` 以下のディレクトリで実行  
同じ階層にある `web/` 以下でローカルサーバーを起動後、 ` ~ run start` のコマンドを実行すると `localhost:3000` でブラウザを起動。

### 必要なパッケージをダウンロード

```
npm i
```

or 

```
yarn install
```

### 圧縮したファイルをテーマディレクトリに移動

```
npm run prod
```

or

```
yarn run prod
```


### 開発時に利用(ソースマップを見る為)

```
npm run start
```

or

```
yarn run start
```

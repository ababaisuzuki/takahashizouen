(function() {

	"use strict";

	/**
	 * [data-tracking-content-type]
	 * [data-tracking-content-id]
	 * [data-tracking-content-name]
	 * [data-tracking-img-src]
	 * [data-tracking-img-label]
	 */
	$('[data-tracking-content-type][data-tracking-content-id][data-tracking-content-name][data-tracking-img-src][data-tracking-img-label]').on('mousedown touchstart', function() {
	    $.ajax({
	        url: '/api/traking/submit',
	        data: {
	            contentType: $(this).attr('data-tracking-content-type'),
	            contentId: $(this).attr('data-tracking-content-id'),
	            contentName: $(this).attr('data-tracking-content-name'),
	            imgSrc: $(this).attr('data-tracking-img-src'),
	            imgLabel: $(this).attr('data-tracking-img-label')
	        }
	    });
	});

}());
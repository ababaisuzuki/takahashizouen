<?php

namespace Bolt\Extension\Bolt\Traking\Controller;

use Silex\Application;
use Silex\ControllerCollection;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * The controller for Traking routes.
 */

class Traking implements ControllerProviderInterface
{
    /** @var Application */
    protected $app;

    /**
     * {@inheritdoc}
     */
    public function connect(Application $app)
    {
        $this->app = $app;

        /** @var ControllerCollection $ctr */
        $ctr = $app['controllers_factory'];

        // This matches both GET requests.
        $ctr->match('tracking', [$this, 'tracking'])
            ->bind('tracking')
            ->method('GET');

        return $ctr;
    }

    /**
     * @param Application $app
     *
     * @return Response
     */
    public function tracking(Application $app)
    {
        var_dump('path traking !');exit;
        $config = $app['tracking.config'];
        $twig = $app['twig'];
        $context = ['entries' => $app['tracking.links']];
        $body = $twig->render($config['template'], $context);

        return new Response($body, Response::HTTP_OK);
    }
}

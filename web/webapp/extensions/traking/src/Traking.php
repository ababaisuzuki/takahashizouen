<?php

namespace Bolt\Extension\Ababai\Traking;

use Bolt\Asset\File\JavaScript;
use Bolt\Controller\Zone;
use Bolt\Extension\SimpleExtension;
use Silex\Application;
use Silex\ControllerCollection;
use Symfony\Component\HttpFoundation\Request;

/**
 * Traking extension class.
 */
class Traking extends SimpleExtension
{
    /**
     * Register routes for the frontend
     *
     * @param ControllerCollection $collection
     */
    protected function registerFrontendRoutes(ControllerCollection $collection)
    {
        $collection->post('/api/traking/submit', [$this, 'submitTraking']);
    }

    protected function registerFrontendControllers()
    {
        return [
            '/traking' => new Controller\TrakingController(),
        ];
    }

    /**
     * Register assets needed for the contactform.
     *
     * @return array
     */
    protected function registerAssets()
    {
        return [
            (
                (new JavaScript('script.js'))
                    ->setLate(true)
                    ->setPriority(99)
                    ->setZone(Zone::FRONTEND)
            ),
        ];
    }

    /**
     * Submit traking.
     *
     * @param Application $application
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function submitTraking(Application $application, Request $request)
    {
        var_dump($request);exit;
        $data = $this->retreiveFormData($request);
        $validation = $this->validateFields($data);

        if(count($validation) > 0) {
            return $application->json([
                'errors' => $validation,
            ], 200);
        }

        return $this->saveTraking($data);
    }

    /**
     * Get form data from the Request
     *
     * @param Request $request
     * @return \stdClass
     */
    private function retreiveFormData(Request $request)
    {
        $data = new \stdClass();

        $data->name = $request->get('name');
        $data->email = $request->get('email');
        $data->telephone = $request->get('telephone');
        $data->message = $request->get('message');

        return $data;
    }

    /**
     * Send the email using the built in mailer.
     *
     * @param $data
     * @return mixed
     */
    private function saveTraking($data)
    {
        try {
            // $message = $this->getContainer()['mailer']
            //     ->createMessage('message')
            //     ->setSubject($this->getConfig()['subject'])
            //     ->setFrom($this->getConfig()['from'])
            //     ->setReplyTo([
            //         $data->email => $data->name,
            //     ])
            //     ->setTo($this->getConfig()['email'])
            //     ->setBody($html, 'text/html')
            //     ->addPart(strip_tags($html), 'text/plain');

            $this->getContainer()['mailer']->send($message);

            $response = $this->getContainer()->json([
                'message' => 'Message Sent!'
            ], 200);

        } catch(\Exception $e) {

            $error = "The 'mailoptions' need to be set in app/config/extensions/contactform.postitief.yml";

            // $this->getContainer()['logger.system']->error($error, ['event' => 'config']);

            $response = $this->getContainer()->json([
                'message' => $error,
            ], 500);
        }

        return $response;
    }

    /**
     * Validate the form fields.
     *
     * @param $data
     * @return array
     */
    private function validateFields($data)
    {
        $errors = [];

        // if (!preg_match("/[-0-9a-zA-Z ]{2,60}/", $data->name)) {
        //     $errors['name'] = 'Naam is een verplicht veld';
        // }

        // if (!preg_match("/[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]/", $data->email)) {
        //     $errors['email'] = 'Email is ongeldig';
        // }

        // if (!preg_match("/[-0-9a-zA-Z .]{2,2000}/", $data->message)) {
        //     $errors['message'] = 'Het bericht mag niet langer zijn dan 2000 karakters.';
        // }

        return $errors;
    }
}

(function() {

	"use strict";

	/**
	 * [data-tracking-content-type]  投稿タイプ
	 * [data-tracking-content-id]    投稿ID
	 * [data-tracking-content-name]  記事名
	 * [data-tracking-img-src]       画像URL
	 * [data-tracking-img-label]     画像キャプション
	 * [data-tracking-field-type]    フィールドタイプ
	 */
	$('[data-tracking-content-type][data-tracking-content-id][data-tracking-content-name][data-tracking-img-src][data-tracking-img-label][data-tracking-field-type]').on('mousedown touchstart', function(e) {

		var self = $(this);console.log(self.attr('data-tracking-img-src'));

	    $.ajax({
	        url: '/save_traking',
	        type: 'GET',
	        data: {
	            contentType: self.attr('data-tracking-content-type'),
	            contentId: self.attr('data-tracking-content-id'),
	            contentName: self.attr('data-tracking-content-name'),
	            imgSrc: self.attr('data-tracking-img-src'),
	            imgLabel: self.attr('data-tracking-img-label'),
	            fieldType: self.attr('data-tracking-field-type')
	        },
            success: function(data) {
                console.log(data);
            },
            error: function () {
                console.log('error');
            }
	    });
	});

}());
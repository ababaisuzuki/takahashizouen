<?php
/**
 * Copyright(C). phonogram,Inc. All Rights Reserved.
 */

// セッション開始
session_name('pfwform');
session_start();

/**
 * JSONデータを出力する。
 *
 * @param mixed $data
 * @return void
 */
if ( ! function_exists('_renderjson')) {
	function _renderjson($data) {
		$json = json_encode($data);
		header('Content-Type: application/json');
		header('Content-Length: ' . strlen($json));
		echo $json;
		exit;
	}
}

// トークン発行
$token = sha1(uniqid(mt_rand(), true));
$_SESSION['pfwtoken'] = $token;
_renderjson($token);

exit;

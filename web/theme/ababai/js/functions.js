$(function(){
// #で始まるアンカーをクリックした場合に処理
$('a[href^=#]').click(function() {
// スクロールの速度
var speed = 800; // ミリ秒
// アンカーの値取得
var href= $(this).attr("href");
// 移動先を取得
var target = $(href == "#" || href == "" ? 'html' : href);
// 移動先を数値で取得
var position = target.offset().top;
// スムーススクロール
$('body,html').animate({scrollTop:position}, speed, 'swing');
return false;
});
});

$(function($){
//sp menu
$(".h-menu").on("click", function() {
$('#sp-global-nav').fadeIn();
});

$(".closebtn").on("click", function() {
$('#sp-global-nav').fadeOut();
});
});


$(function($){
//sp menu
$(".pc-menu").on("click", function() {
$('#pc-menu-open').fadeIn();
});

$(".closebtn").on("click", function() {
$('#pc-menu-open').fadeOut();
});
});


$(function(){
var device = navigator.userAgent;
if((device.indexOf('iPhone') > 0 && device.indexOf('iPad') == -1) || device.indexOf('iPod') > 0 || device.indexOf('Android') > 0){
$(".sp-tel-link").wrap('<a href="tel:0120000000"></a>');
}
});

$(function(){
$('.gallery__list').magnificPopup({
delegate: 'a',
type: 'image',
gallery: { //ギャラリー表示にする
enabled:true
},
image: {
titleSrc: function(item) {
return item.el.find('img').attr('alt');
}
}
});
});

$("#gl-navi li").hover(function() {
$(this).children('ul').show();
}, function() {
$(this).children('ul').hide();
});


$(function(){
$('.pop-img').magnificPopup({
delegate: 'a',
type: 'image',
gallery: { //ギャラリー表示にする
enabled:true
},
image: {
titleSrc: function(item) {
return item.el.find('img').attr('alt');
}
}
});

});



$(function() {

// ★ Before ここから
// サムネイルのサイズ
var thumbWidth = 56;
var thumbHeight = 56;

// サムネイルの作成
var insert = '';
for (var i = 0; i < $('#before-slider li').length; i++) {
insert += '<a data-slide-index="' + i + '" href="#"><img src="' + $('#before-slider li').eq(i).children('img').attr('src') + '" width="' + thumbWidth + '" height="' + thumbHeight + '" /></a>';
};

$('.custom-thumb').append(insert);

var slider01 = $('#before-slider').bxSlider({
mode: 'fade',
controls: false,
pagerCustom: '.custom-thumb',
onSlideBefore: function($slideElement, oldIndex, newIndex) {
slider02.goToSlide(newIndex);
}
});

// ★ After ここから

// サムネイルのサイズ
var thumbWidth = 56;
var thumbHeight = 56;

// サムネイルの作成
var insert = '';
for (var i = 0; i < $('#after-slider li').length; i++) {
insert += '<a c data-slide-index="' + i + '" href="#"><img src="' + $('#after-slider li a').eq(i).children('img').attr('src') + '" width="' + thumbWidth + '" height="' + thumbHeight + '" /></a>';
};

$('.custom-thumb-02').append(insert);

var slider02 = $('#after-slider').bxSlider({
mode: 'fade',
pagerCustom: '.custom-thumb-02',
controls: false,
onSlideBefore: function($slideElement, oldIndex, newIndex) {
slider01.goToSlide(newIndex);
}
});

});

$(function() {
$('time').each(function() {
var date = $(this).attr('datetime');
var dateNew = date.replace(/\//g,'-');
$(this).attr('datetime',dateNew);
});

});

$(function(){
hsize = $(window).height();

$(".fix-nav-01").on("click", function() {
$('#open_nav').fadeIn();
});
$("#open_nav .closebtn").on("click", function() {
$('#open_nav').fadeOut();
});

});

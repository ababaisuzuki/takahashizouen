<?php
/**
 * Copyright(C). phonogram,Inc. All Rights Reserved.
 */

// セッション開始
session_name('pfwform');
session_start();

// ライブラリ読み込み
require_once(dirname(__FILE__) . '/inc/php/phpmailer/PHPMailerAutoload.php');

/**
 * 開発環境かどうか判定する。
 * 開発環境の場合は true を、それ以外の場合は false を返す。
 *
 * @return bool
 */
if ( ! function_exists('_isdev')) {
	function _isdev() {
		if (preg_match('/\.phonogram\.(tv|jp)\//iu', __FILE__)) {
			return true;
		}
		return false;
	}
}

/**
 * 別ページにリダイレクトする。
 *
 * @param string $url
 * @return void
 */
if ( ! function_exists('_redirect')) {
	function _redirect($url) {
		header('Location: ' . $url, true, 302);
		exit;
	}
}

/**
 * テンプレートからメール本文を生成する。
 *
 * @param string $template
 * @param array $params
 * @return string
 */
if ( ! function_exists('_createmailbody')) {
	function _createmailbody($template, $params = array()) {
		$paramstext = '';
		$valueSeparator = PHP_EOL;
		if (is_array($params)) {
			foreach ($params as $key => $value) {

				if ( $key == '姓' ) {//メール本文の項目を調整
					$paramstext .= sprintf('[お名前]%s%s%s%s', PHP_EOL, $params['姓'], $params['名'], str_repeat(PHP_EOL, 2));
					continue;
				} elseif ( $key == 'せい' ) {
					$paramstext .= sprintf('[おなまえ]%s%s%s%s', PHP_EOL, $params['せい'], $params['めい'], str_repeat(PHP_EOL, 2));
					continue;
				} elseif ( $key == '郵便番号_1' ) {
					$paramstext .= sprintf('[郵便番号]%s%s-%s%s', PHP_EOL, $params['郵便番号_1'], $params['郵便番号_2'], str_repeat(PHP_EOL, 2));
					continue;
				} elseif ( $key == '電話番号_1') {
					$paramstext .= sprintf('[電話番号]%s%s-%s-%s%s', PHP_EOL, $params['電話番号_1'], $params['電話番号_2'], $params['電話番号_3'], str_repeat(PHP_EOL, 2));
					continue;
				}

				if ( strpos($key, '郵便番号') === false &&  //条件式の値はメール本文に入れない
					 strpos($key, '電話番号') === false &&
					 strpos($key, '名') === false       &&
					 strpos($key, 'めい') === false     &&
					 strpos($key, 'username') === false &&
					 strpos($key, 'cms_relation_code') === false   ) {
					if (is_array($value)) {
						$paramstext .= sprintf('[%s]%s%s%s', $key, PHP_EOL, implode($valueSeparator, $value), str_repeat(PHP_EOL, 2));
					} else {
						$paramstext .= sprintf('[%s]%s%s%s', $key, PHP_EOL, $value, str_repeat(PHP_EOL, 2));
					}
				}
			}
		}
		return preg_replace('/<%\s*フォーム入力内容\s*%>/u', $paramstext, $template);
	}
}

/**
 * メールを送信する。
 *
 * @param array $options
 * @param array $params
 * @return bool
 */
if ( ! function_exists('_sendemail')) {
	function _sendemail($options, $params = array()) {
		if (empty($options['to']['address'])) {
			return true;
		}
		$mail = new PHPMailer();
		$mail->CharSet = 'UTF-8';
		$mail->Encoding = 'base64';
		$mail->setFrom($options['from']['address'], $options['from']['name']);
		$mail->addAddress($options['to']['address'], $options['to']['name']);
		foreach($options['bcc'] as $bcc) {
			$mail->addBCC($bcc['address'], $bcc['name']);
		}
		$mail->Subject = (_isdev() ? '【テスト環境】' : '') . $options['subject'];
		$mail->Body = _createmailbody($options['body'], $params);
		foreach ($files as $key => $value) {
			$mail->addAttachment($key, $value);
		}
		$result = $mail->send();
		return $result;
	}
}

// 入力パラメータを配列に格納
$params = $_POST;

// 各種設定
$settings = require(dirname(__FILE__) . '/pfwform-config.php');

// POST送信以外は受け付けない
if (strtoupper($_SERVER['REQUEST_METHOD']) !== 'POST') {
	_redirect($settings['pages']['error']);
}

// トークン検証
if (empty($_SESSION['pfwtoken']) || ($_SESSION['pfwtoken'] != $params['pfwtoken'])) {
	_redirect($settings['pages']['error']);
}

// トークン破棄
unset($_SESSION['pfwtoken'], $params['pfwtoken']);

/**************************
 * CRMにデータを登録 START
 **************************/
// CRMに登録するデータをセット
$api_params = array(
	'username'          => $params["username"],
	'cms_relation_code' => $params["cms_relation_code"],
	'last_name'         => $params["姓"],
	'first_name'        => $params["名"],
	'ph_last_name'      => $params["せい"],
	'ph_first_name'     => $params["めい"],
	'first_tel'         => $params["電話番号_1"],
	'middle_tel'        => $params["電話番号_2"],
	'last_tel'          => $params["電話番号_3"],
	'first_postal_code' => $params["郵便番号_1"],
	'last_postal_code'  => $params["郵便番号_2"],
	'address'           => $params["ご住所"],
	'mail'              => $params["メールアドレス"],
	'office_memo'       => _createmailbody($settings['email']['admin']['body'], $params),
	'message'           => _createmailbody($settings['email']['admin']['body'], $params),
);

// 空の要素を一括で削除。（空のままPOSTしてしまうと、CRMに既に入っているデータが消えてしまうので）
$api_params = array_filter($api_params, 'strlen');

// APIにデータをPOST
if (_isdev()) {
	$postdata_url = 'https://www.abaluck.com/crm/user/apis/i_receive_event_participants';
} else {
	$postdata_url = 'https://www.abaluck.com/crm/user/apis/i_receive_event_participants';
}

$headers = array(
	'Content-Type: application/x-www-form-urlencoded',
);
$options = array(
'http' => array(
'method' => 'POST',
'content' => http_build_query($api_params),
'header' => implode("\r\n", $headers),
),
'ssl' => array(
'verify_peer' => false,
'verify_peer_name' => false,
),
);
$contents = file_get_contents($postdata_url, FALSE, stream_context_create($options));
/*
 * CRM連携が失敗しても処理を継続する。（単純なメールフォームにプラスサブ機能的な位置づけの為。）
if ( $contents === FALSE ) {
	// POSTリクエスト失敗
	_redirect($settings['pages']['error']);
}
$api_result = json_decode($contents, TRUE);

if($api_result["error"] !== 0)
{
	// データ登録失敗
	_redirect($settings['pages']['error']);
}
*/
/**************************
 * CRMにデータを登録 END
 **************************/

// メール送信（管理者宛て）
if ( ! _sendemail($settings['email']['admin'], $params)) {
	_redirect($settings['pages']['error']);
}

// メール送信（利用者宛て）
if ( ! _sendemail($settings['email']['replay'], $params)) {
	_redirect($settings['pages']['error']);
}

// サンクスページへリダイレクト
_redirect($settings['pages']['complete']);

exit;
